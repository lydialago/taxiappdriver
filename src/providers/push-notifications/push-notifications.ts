import { Injectable } from '@angular/core';
import {OneSignal, OSNotification, OSNotificationPayload} from '@ionic-native/onesignal';
import {
  Platform,
  LoadingController,
  AlertController,
  ModalController,
  NavController,
  App,
  ToastController
} from "ionic-angular";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { DriverInfoProvider } from "../driver-info/driver-info";
import * as Constants from "../../interfaces/constants";
import {DetalleServicioPage} from "../../pages/detalle-servicio/detalle-servicio";
import {Servicio} from "../../interfaces/servicio.interface";
import {TabsPage} from "../../pages/tabs/tabs";
import {Subject} from "rxjs/Subject";
import {Location} from "../../interfaces/location";
import {UserDTO} from "../../interfaces/userDTO";
import {ConvertirFechaStringProvider} from "../convertir-fecha-string/convertir-fecha-string";

/**
 * Provider for receiving and manage Push Notificacions using OneSignal.
 */
@Injectable()
export class PushNotificationsProvider {

  /** Object Servicio with information about the service. */
  servicio: Servicio;
  /** Instance of NavController (NavController cannot be used directly into a provider).*/
  private nav: NavController;
  /** Observable to subscribe from another pages */
  public notificationObservable = new Subject<any>();

  /**
   * Constructor of PushNotificacions provider
   *
   * @param {App} app
   * @param {OneSignal} oneSignal OneSignal native plugin which is an client implementation for using the OneSignal Service.
   * @param {Platform} platform Default service used to get information about the current device.
   * @param {LoadingController} loadingCtrl Default component which is an overlay that can be used to indicate activity while blocking user interaction.
   * @param {AlertController} alertCtrl Default component which is a dialog that presents users with information or collects information from the user using inputs.
   * @param {ModalController} modalCtrl Default component which is a content pane that goes over the user's current page.
   * @param {ToastController} toastCtrl Default component for subtle notification commonly used in modern applications. It can be used to provide feedback about an operation or to display a system message.
   * @param {HttpClient} http Cordova / Phonegap plugin for communicating with HTTP servers.
   * @param {DriverInfoProvider} _driverInfo Custom provider to create a driver object, set and obtain information.
   * @param {ConvertirFechaStringProvider} _convertirFechaString Custom provider to transform a datetime string into a sentence like "Hoy a las 12:00".
   */
  constructor(
    private app: App,
    private oneSignal: OneSignal,
    private platform: Platform,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private modalCtrl: ModalController,
    private toastCtrl: ToastController,
    private http: HttpClient,
    private _driverInfo: DriverInfoProvider,
    private _convertirFechaString: ConvertirFechaStringProvider
  ) {
    this.nav = app.getActiveNav();
  }

  /**
   * Main function to set up OneSignal and handle notifications received and opened.
   * @returns void.
   */
  init_notifications(){

    if(this.platform.is("cordova")){

      this.oneSignal.startInit('176a6447-7609-42c7-93fe-a039605e8ffc', '534604254331');

      this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);

      this.oneSignal.handleNotificationReceived()
        .subscribe((data: any) => {
          console.log("Recibe notificacion");
          this.onPushReceived(data.payload);
        });

      this.oneSignal.handleNotificationOpened()
        .subscribe( (data: any) => {
          this.onPushOpened(data.payload);
        });

      this.oneSignal.endInit();
    }

    else{
      console.log("OneSignal no funciona en Chrome");
    }
  }

  /**
   * Function to manage notifications when received. By now, there are 4 types of push notifications:
   * 1) Notification of new service that the driver can accept or reject. If he does nothing the alert disappear after 45 seconds and the notification is sent to another driver.
   * 2) Notification of the driver leaves the pick up point. The client is been warned that the driver is at pick up point but he is about to leave. The client can ask the driver to wait or cancel the service.
   * 3) Notification of service lost so the driver it's free again.
   * 4) Notification of pending services every minute to update the list of pending services of the app.
   * @param {OSNotificationPayload} payload Object received inside the notification push.
   * @returns void.
   */
  private onPushReceived(payload: OSNotificationPayload){
    let notification_type: string = payload.additionalData.notificationType;

    console.log("NOTIFICACION RECIBIDA: " + JSON.stringify(payload.additionalData));
    switch(notification_type) {
      case Constants.NOTIFICATION_TYPE_BOOKING:
        console.log("Notificacion booking");
        let alert = this.alertCtrl.create({
          title: '¡Nuevo servicio!',
          message: "<p>Punto de recogida: <strong>" + payload.additionalData.data.origin.address + "</strong></p>"
          + "<p>Hora: <strong>" + this._convertirFechaString.formateoHoraRecogida(payload.additionalData.data) + "</strong></p>"
          + "<p>Método de pago: <strong>" + payload.additionalData.data.bookingOptions.payment + "</strong></p>",
          enableBackdropDismiss: false,
          buttons: [
            {
              text: 'Rechazar',
              handler: () => {
                console.log('Peticion rechazada');
                clearTimeout(timer);
                this.http.get(Constants.SERVER_IP + "/driver/" + this._driverInfo.driver.uid + "/reject/" + payload.additionalData.data.id, {
                headers: new HttpHeaders().set('Content-Type', 'application/json')
                }).subscribe((res: any) => {
                  console.log(res);
                }, (err) => {
                  console.log(err);
                });
              }
            },
            {
              text: 'Aceptar',
              handler: () => {
                console.log('Peticion aceptada');
                clearTimeout(timer);
                this.http.get(Constants.SERVER_IP + "/driver/" + this._driverInfo.driver.uid + "/accept/" + payload.additionalData.data.id, {
                headers: new HttpHeaders().set('Content-Type', 'application/json')
                }).subscribe((res: any) => {
                  let dealId = res.dealId;
                  let modal = this.modalCtrl.create( DetalleServicioPage, {
                    servicio : payload.additionalData.data,
                    dealId: dealId
                  });
                  modal.present();
                }, (err) => {
                  console.log(err);
                  let toast = this.toastCtrl.create({
                    message: 'No se ha podido aceptar el servicio. Inténtalo de nuevo más tarde.',
                    showCloseButton : true,
                    position: 'bottom'
                  });
                  toast.present();
                });
              }
            }
          ]
        });
        alert.present();
        let timer = setTimeout(() => {
            alert.dismiss();
        },45000);

        break;

      case Constants.NOTIFICATION_TYPE_DRIVER_LEAVE:
        console.log(JSON.stringify(payload.additionalData));
        this.notificationObservable.next( { shouldLeave: payload.additionalData.data.shouldLeave });
        console.log("Notificacion el conductor se va, espera o no");
        if(payload.additionalData.data.shouldLeave){
          alert = this.alertCtrl.create({
            title: 'Servicio cancelado',
            message: 'Lo sentimos, el cliente ha cancelado el servicio. Se le cobrará una comisión.',
            enableBackdropDismiss: false,
            buttons: [
              {
                text: 'OK',
                handler: () => {
                  console.log("servicio cancelado");
                  this._driverInfo.updateStatus(Constants.STATUS_FREE);
                }
              }
            ]
          });
          alert.present();
        }
        else{
          alert = this.alertCtrl.create({
            title: 'Por favor, espera...',
            message: 'El cliente aún no ha llegado al punto de recogida pero necesita el taxi.',
            enableBackdropDismiss: false,
            buttons: [
              {
                text: 'OK',
                handler: () => {
                  console.log("servicio esperaaaaaaa");
                }
              }
            ]
          });
          alert.present();
        }
        break;

      case Constants.NOTIFICATION_TYPE_LOST:
        console.log("notificacion servicio cancelado");
        this.notificationObservable.next({ lost: true });
        this._driverInfo.updateStatus(Constants.STATUS_FREE);
        alert = this.alertCtrl.create({
          title: 'Servicio cancelado',
          message: 'El cliente ha cancelado el servicio. Lo sentimos.',
          enableBackdropDismiss: false,
          buttons: [
            {
              text: 'OK'
            }
          ]
        });
        alert.present();
        break;

      case Constants.NOTIFICATION_TYPE_PENDINGS:
        console.log("notificacion servicios pendientes");
        let serviciosPendientes = payload.additionalData.data;
        this.notificationObservable.next({ pendingServices: serviciosPendientes });
    }

  }

  /**
   * Unused function to handle notification when they're opened.
   * @param {OSNotificationPayload} payload Object received inside the notification push.
   * @returns void.
   */
  private onPushOpened(payload: OSNotificationPayload){
    console.log("valores de la notificacion abierta: " + payload);
    let alert = this.alertCtrl.create({
      title: 'Informacion',
      message: JSON.stringify(payload.additionalData)
    });
    alert.present();
  }

}
