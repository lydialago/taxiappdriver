import { Injectable } from '@angular/core';
import {Servicio} from "../../interfaces/servicio.interface";

/**
 * Provider for formatting a datetime to a string like "Hoy a las 12:00" or "El 4 de junio a las 13.30".
 */
@Injectable()
export class ConvertirFechaStringProvider {

  /**
   * Function that receives a service, takes the pick up datetime and formats it to be shown in the page html.
   * @param {Servicio} servicio The service the date from which it would be formatted
   * @returns {string} The string to be displayed on the html page
   */
  formateoHoraRecogida( servicio: Servicio){
    let div = servicio.pickupAt.split(" ");
    let fechaServicio = div[0].split("-");
    let horaServicio = div[1].split(":");
    let meses : string[] = ["", "enero", "febrero", "marzo", "abril", "mayo", "junio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"];
    let fechaActual : Date = new Date();
    let resultado : string = "";
    //incluir fechaActual.getMonth()-1 porque los meses cuentan a partir de 0
    if(parseInt(fechaServicio[2]) == fechaActual.getDate() && parseInt(fechaServicio[1]) == fechaActual.getMonth() && parseInt(fechaServicio[0]) == fechaActual.getFullYear()){
        resultado += "Hoy";
    }
    else if(parseInt(fechaServicio[2]) == fechaActual.getDate()+1 && parseInt(fechaServicio[1]) == fechaActual.getMonth() && parseInt(fechaServicio[0]) == fechaActual.getFullYear()){
        resultado = "Mañana"
    }
    else{
        resultado = "El " + fechaServicio[2] + " de " + meses[parseInt(fechaServicio[1])];
    }
    resultado += " a las " + horaServicio[0] + ":" + horaServicio[1];

    return resultado;
  }

}
