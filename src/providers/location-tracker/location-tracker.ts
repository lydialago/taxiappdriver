import { Injectable, NgZone } from '@angular/core';
import {BackgroundGeolocation, BackgroundGeolocationConfig} from "@ionic-native/background-geolocation";
import { Geolocation, Geoposition } from "@ionic-native/geolocation";
import 'rxjs/add/operator/filter';

/** Firebase */
import * as firebase from 'firebase/app';
import { AngularFireAuth } from "angularfire2/auth";
import {HttpClient} from "@angular/common/http";
import {DriverInfoProvider} from "../driver-info/driver-info";

/**
 * Provider for tracking the location of the driver in background and foreground. Using native Cordova plugins BackgroundGeolocation and Geolocation.
 */
@Injectable()
export class LocationTrackerProvider {

  /** Variable to subscribe or unsubscribe for getting the current position in foreground */
  public watch: any;
  /** Latitude coordinate */
  public lat: number = 0;
  /** Longitude coordinate */
  public lng: number = 0;

  /**
   * Constructor of the LocationTracker provider
   *
   * @param {NgZone} zone Angular NgZone. This special zone extends the basic functionality of a zone to facilitate change detection.
   * @param {Geolocation} geolocation Native Cordova plugin for geolocation. Provides information about the device's location, such as latitude and longitude.
   * @param {BackgroundGeolocation} background Geolocation Native Cordova plugin for background geolocation. Provides foreground and background geolocation with battery-saving "circular region monitoring" and "stop detection".
   * @param {AngularFireAuth} afAuth Firebase Authentication module
   * @param {DriverInfoProvider} _driverInfo Custom provider for obtain and update the driver information from Firebase.
   */
  constructor(public zone: NgZone,
              public geolocation: Geolocation,
              public backgroundGeolocation: BackgroundGeolocation,
              public afAuth: AngularFireAuth,
              public _driverInfo: DriverInfoProvider
  ) {}

  /**
   * Main function to start background and foreground tracking position and save it into Firebase and Spring server databases.
   * @returns void
   */
  startTracking() {

    // Background Tracking

    let config: BackgroundGeolocationConfig = {
      desiredAccuracy: 10,
      stationaryRadius: 20,
      distanceFilter: 10,
      debug: false,
      interval: 1000,
      stopOnTerminate: true,
      startForeground: true
    };

    this.backgroundGeolocation.configure(config).subscribe((location) => {

      console.log('BackgroundGeolocation:  ' + location.latitude + ',' + location.longitude);

      // Run update inside of Angular's zone
      this.zone.run(() => {
        this.lat = location.latitude;
        this.lng = location.longitude;
        this.afAuth.authState.subscribe(data => {
          if(data){
            //guardar localizacion usuario en firebase
            firebase.database()
              .ref('/drivers')
              .child(data.uid)
              .update({
                location: {
                  lat: this.lat,
                  lng: this.lng
                }
              });
            //se guarda en spring server
            this._driverInfo.updateLocation(String(this.lat), String(this.lng));
          }
        });
      });

    }, (err) => {

      console.log(err);

    });

    // Turn ON the background-geolocation system.
    this.backgroundGeolocation.start();

    // Foreground Tracking

    let options = {
      frequency: 1000,
      enableHighAccuracy: true
    };

    this.watch = this.geolocation.watchPosition(options).filter((p: any) => p.code === undefined).subscribe((position: Geoposition) => {

      // Run update inside of Angular's zone
      this.zone.run(() => {
        this.lat = position.coords.latitude;
        this.lng = position.coords.longitude;
        this.afAuth.authState.subscribe(data => {
          if(data){
            //guardar localizacion usuario en firebase
            firebase.database()
              .ref('/drivers')
              .child(data.uid)
              .update({
                location: {
                  lat: this.lat,
                  lng: this.lng
                }
              });
          }
        });
      });

    });
  }

  /**
   * Function to stop tracking location triggered when the driver log out.
   * @returns void
   */
  stopTracking() {
    console.log('stopTracking');
    this.backgroundGeolocation.finish();
    this.watch.unsubscribe();
  }

}
