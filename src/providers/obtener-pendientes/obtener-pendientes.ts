import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as Constants from "../../interfaces/constants";
import {OrdenarServiciosProvider} from "../ordenar-servicios/ordenar-servicios";
import {ToastController} from "ionic-angular";

/**
 * Provider for getting all the pending services from the Spring Server (pending services are those that have not been assigned to any driver).
 */
@Injectable()
export class ObtenerPendientesProvider {

  /**
   * Constructor of the ObtenerPendientes provider.
   *
   * @param {HttpClient} http Cordova / Phonegap plugin for communicating with HTTP servers.
   * @param {OrdenarServiciosProvider} _ordenarServicios Custom provider to sort services by datetime.
   * @param {ToastController} toastCtrl Default component for subtle notification commonly used in modern applications. It can be used to provide feedback about an operation or to display a system message.
   */
  constructor(
    public http: HttpClient,
    public _ordenarServicios: OrdenarServiciosProvider,
    public toastCtrl: ToastController) {}

  /**
   * Function to get all the pending services from the Spring server.
    * @returns {Promise<any[]>} Returns an observable to which you can subscribe from any page
   */
  obtenerServiciosPendientes(): Promise<any[]> {
    return this.http.get<any[]>(Constants.SERVER_IP + "/booking/pendings", {
        headers: new HttpHeaders().set('Content-Type', 'application/json')
    }).toPromise();
  }
}
