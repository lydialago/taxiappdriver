import { Injectable } from '@angular/core';
import { Driver } from "../../interfaces/driver";
import { AngularFireAuth } from "angularfire2/auth";
import * as firebase from 'firebase/app';
import * as Constants from "../../interfaces/constants";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";

/**
 * Provider for obtain and update the driver information from Firebase.
 */
@Injectable()
export class DriverInfoProvider {

  /** Driver object with all the information from database. */
  public driver = {} as Driver;

  /**
   * Constructor of the DriverInfo provider
   * @param {AngularFireAuth} afAuth Firebase Authentication module
   * @param {HttpClient} http Cordova / Phonegap plugin for communicating with HTTP servers.
   */
  constructor(private afAuth: AngularFireAuth, public http: HttpClient) {}

  /**
   * Function to get current user information authenticated from Firebase and keep it up to date.
   * @returns void
   */
  obtainUserInfoFromFirebase (){
    this.driver.uid = this.afAuth.auth.currentUser.uid;
        firebase.database()
          .ref('/drivers')
          .child(this.driver.uid).on('value', data => {
            this.driver.email = data.val().email;
            this.driver.lastname = data.val().lastname;
            this.driver.name = data.val().name;
            this.driver.phone = data.val().phone;
            this.driver.status = data.val().status;
            this.driver.location = data.val().location;
        });
  }

  /**
   * Function to update driver status on Firebase and on the server.
   * @param status
   */
  updateStatus(status){
    let driverStatusServer = [Constants.DRIVER_STATUS_OFFLINE, Constants.DRIVER_STATUS_BUSY, Constants.DRIVER_STATUS_ONLINE, Constants.DRIVER_STATUS_REST ];
    //se actualiza en firebase
    firebase.database()
      .ref('/drivers')
      .child(this.driver.uid)
      .update({
        status: status
      });

    //se actualiza en el servidor
    this.http.post(Constants.SERVER_IP + "/driver/" + this.driver.uid + "/status", { 'status' : driverStatusServer[status] },{
      headers: new HttpHeaders().set('Content-Type', 'application/json')
    }).subscribe((res: any) => {
      console.log(res);
    }, (err) => {
      console.log(err);
    });
  }

  /**
   * Function to update driver Location in spring server.
   * @param {string} lat Latitude coordinate
   * @param {string} lng Longitude coordinate
   */
  updateLocation(lat: string, lng: string) {
    this.http.post(Constants.SERVER_IP + "/driver/" + this.driver.uid + "/location", {lat: lat, lng: lng}, {headers: new HttpHeaders().set('Content-Type', 'application/json')})
      .subscribe(
        res => console.log("Posición actualizada con Spring server"),
        error => console.log("Error al actualizar posicion con spring. " + JSON.stringify(error))
      );
  }

}
