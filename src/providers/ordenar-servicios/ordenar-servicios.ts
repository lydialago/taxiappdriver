import { Injectable } from '@angular/core';
import {Servicio} from "../../interfaces/servicio.interface";

/**
 * Provider for sort services by datetime.
 */
@Injectable()
export class OrdenarServiciosProvider {

  /**
   * Function that returns a vector of ordered services from futures to past.
   * @param {Servicio[]} servicios Array of services.
   * @returns {Servicio[]} Array of services sorted.
   */
    ordenarServiciosPrimeroFuturos(servicios: Servicio[]){
      return servicios.sort((a, b) => Date.parse(a.pickupAt) <= Date.parse(b.pickupAt) ? 1 : -1);
    }

  /**
   * Function that returns a vector of ordered services from past to futures.
   * @param {Servicio[]} servicios Array of services.
   * @returns {Servicio[]} Array of services sorted.
   */
    ordenarServiciosPrimeroPasados(servicios: Servicio[]){
        return servicios.sort((a, b) => Date.parse(a.pickupAt) <= Date.parse(b.pickupAt) ? -1 : 1);
    }
}
