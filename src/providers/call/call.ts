import { Injectable } from '@angular/core';
import { CallNumber } from '@ionic-native/call-number';

/**
 * Provider for making calls using the native Cordova plugin CallNumber
 */
@Injectable()
export class CallProvider {

  /**
   * Constructor for the CallProvider
    * @param {CallNumber} callNumber Native Cordova plugin to call a number directly from a Cordova/Ionic application.
   */
  constructor(public callNumber: CallNumber) {}

  /**
   * Function to call directly from the app
   * @param phone String with the phone number
   */
  llamar(phone){
      this.callNumber.callNumber(phone, true)
          .then(res => console.log('Launched dialer!', res))
          .catch(err => console.log('Error launching dialer', err));
  }

}
