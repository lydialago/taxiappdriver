import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Geolocation } from '@ionic-native/geolocation';
import { BackgroundGeolocation } from "@ionic-native/background-geolocation";
import { OneSignal } from '@ionic-native/onesignal';
import { ScreenOrientation } from "@ionic-native/screen-orientation";

/* Paginas */
import { MyApp } from './app.component';
import { LoginPage } from "../pages/login/login";
import { HomePage } from '../pages/home/home';
import { HistorialPage } from "../pages/historial/historial";
import { TabsPage } from "../pages/tabs/tabs";
import { AjustesPage } from "../pages/ajustes/ajustes";
import { MisServiciosPage } from "../pages/mis-servicios/mis-servicios";
import { PendientesPage } from "../pages/pendientes/pendientes";
import { ProgramadosPage } from "../pages/programados/programados";
import { DetalleServicioPage } from "../pages/detalle-servicio/detalle-servicio";

/*Providers*/
import { ConvertirFechaStringProvider } from '../providers/convertir-fecha-string/convertir-fecha-string';
import { OrdenarServiciosProvider } from '../providers/ordenar-servicios/ordenar-servicios';
import { CallProvider } from '../providers/call/call';
import { CallNumber } from "@ionic-native/call-number";
import { DriverInfoProvider } from '../providers/driver-info/driver-info';
import { LocationTrackerProvider } from '../providers/location-tracker/location-tracker';
import { PushNotificationsProvider } from "../providers/push-notifications/push-notifications";
import { HttpClientModule } from "@angular/common/http";
import { ObtenerPendientesProvider } from "../providers/obtener-pendientes/obtener-pendientes";

/** Firebase*/
import { environment } from "../environments/environment";
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';

@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    HomePage,
    HistorialPage,
    TabsPage,
    AjustesPage,
    MisServiciosPage,
    PendientesPage,
    ProgramadosPage,
    DetalleServicioPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    HttpClientModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    HomePage,
    HistorialPage,
    TabsPage,
    AjustesPage,
    MisServiciosPage,
    PendientesPage,
    ProgramadosPage,
    DetalleServicioPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Geolocation,
    BackgroundGeolocation,
    CallNumber,
    OneSignal,
    ScreenOrientation,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ConvertirFechaStringProvider,
    OrdenarServiciosProvider,
    CallProvider,
    DriverInfoProvider,
    LocationTrackerProvider,
    PushNotificationsProvider,
    ObtenerPendientesProvider
  ]
})
export class AppModule {}
