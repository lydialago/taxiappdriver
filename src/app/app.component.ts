import { Component, Inject } from '@angular/core';
import { Platform, MenuController, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ScreenOrientation } from "@ionic-native/screen-orientation";

/** Paginas */
import { TabsPage } from '../pages/tabs/tabs';
import { HistorialPage } from "../pages/historial/historial";
import { AjustesPage } from "../pages/ajustes/ajustes";
import { LoginPage } from "../pages/login/login";

/** Firebase */
import { AngularFireAuth } from "angularfire2/auth";
import * as firebase from 'firebase/app';
import { FirebaseApp } from "angularfire2";

/** Providers */
import {LocationTrackerProvider} from "../providers/location-tracker/location-tracker";
import { DriverInfoProvider } from "../providers/driver-info/driver-info";
import { PushNotificationsProvider } from "../providers/push-notifications/push-notifications";

/**
 * Main component of the App
 */
@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  /** Reference to LoginPage */
  rootPage:any = LoginPage;
  /** Reference to HistorialPage */
  historial:any = HistorialPage;
  /** Reference to AjustesPage */
  ajustes:any = AjustesPage;

  /**
   * Constructor of the App component. It checks if the user is logged in. If it's true, the root page of the aplication is the TabsPage, and if it's not, the root is the LoginPage. This is useful to prevent the user from having to login every time they open the app.
   *
   * @param firebaseApp Native plugin that works with push notifications, authentication, analytics, event tracking, crash reporting and more from Google Firebase.
   * @param {Platform} platform Default service used to get information about the current device.
   * @param {StatusBar} statusBar Native component which manage the appearance of the native status bar.
   * @param {SplashScreen} splashScreen Native plugin which displays and hides a splash screen during application launch.
   * @param {MenuController} menuCtrl Default provider which makes it easy to control a Menu. Its methods can be used to display the menu, enable the menu, toggle the menu, and more.
   * @param {AlertController} alertCtrl Default component which is a dialog that presents users with information or collects information from the user using inputs.
   * @param {AngularFireAuth} afAuth Firebase Authentication module
   * @param {LocationTrackerProvider} _locationTracker Custom provider for tracking background and foreground device geolocation.
   * @param {DriverInfoProvider} _driverInfo Custom provider to create a driver object, set and obtain information.
   * @param {PushNotificationsProvider} _pushNotif Custom provider to manage push notifications.
   * @param {ScreenOrientation} screenOrientation Native plugin to set/lock the screen orientation in a common way.
   */
  constructor(
    @Inject(FirebaseApp) firebaseApp: any,
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public menuCtrl: MenuController,
    public alertCtrl: AlertController,
    public afAuth: AngularFireAuth,
    public _locationTracker: LocationTrackerProvider,
    public _driverInfo: DriverInfoProvider,
    public _pushNotif: PushNotificationsProvider,
    public screenOrientation: ScreenOrientation
  ) {
    this.initializeApp();
    const unsubscribe = firebase.auth().onAuthStateChanged(user => {
      console.log(user);
      if (!user) {
        this.rootPage = LoginPage;
        unsubscribe();
      } else {
        this.rootPage = TabsPage;
        unsubscribe();
      }
    });
  }

  /**
   * Function called when the app launches. If the user is logged in it starts tracking geolocation, obtain the driver info from firebase and start open signal service to receive push notifications.
   * @returns void.
   */
  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      if(this.platform.is("android")){
        this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
      }
      this.afAuth.authState.subscribe(data => {
        if(data){
          console.log("ID usuario: " + data.uid + " Email: " + data.email);
          console.log("start tracking");
          this._locationTracker.startTracking();
          this._driverInfo.obtainUserInfoFromFirebase();
          this._pushNotif.init_notifications();
        }
      });
    });
  }

  /**
   * Function to navigate to another page passed by parameter.
   * @param pagina Page to navigate.
   * @returns void.
   */
  openPage(pagina :any) {
      this.rootPage = pagina;
      this.menuCtrl.close();
  }

  /**
   * Function to logout. It uses Firebase logout method, stops tracking geolocation and navigate to login page.
   * @returns void.
   */
  logout(){
    let alert = this.alertCtrl.create({
      title : "Salir",
      message : "¿Estás seguro de que deseas salir?",
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancelar',
        },
        {
          text: 'Aceptar',
          handler: () => {
            firebase.auth().signOut();
            this.menuCtrl.close();
            this.rootPage = LoginPage;
            this._locationTracker.stopTracking();
          }
        }
      ]
    });
    alert.present();
  }
}

