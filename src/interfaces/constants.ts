/**
 * Interfaces for constants used in the whole app.
 */
export const CLIENT_ROLE: string = "client";
export const DRIVER_ROLE: string = "driver";

export const STATUS_OFF: number = 0;
export const STATUS_BUSY: number = 1;
export const STATUS_FREE: number = 2;
export const STATUS_BREAK: number = 3;

export const DRIVER_STATUS_OFFLINE = "OFFLINE";
export const DRIVER_STATUS_BUSY = "BUSY";
export const DRIVER_STATUS_ONLINE = "ONLINE";
export const DRIVER_STATUS_REST = "REST";

export const SERVER_IP: string = "http://192.168.43.83:8080";

export const NOTIFICATION_TYPE_PENDINGS: string = "PENDINGS";
export const NOTIFICATION_TYPE_SCHEDULED: string = "SCHEDULED";
export const NOTIFICATION_TYPE_BOOKING: string = "BOOKING";
export const NOTIFICATION_TYPE_DRIVER: string = "DRIVER";
export const NOTIFICATION_TYPE_DRIVER_LEAVE: string = "DRIVER_LEAVE";
export const NOTIFICATION_TYPE_ON_DOOR: string = "ON_DOOR";
export const NOTIFICATION_TYPE_LOST: string = "LOST";
export const NOTIFICATION_TYPE_BOOKING_ADVICE: string = "BOOKING_ADVICE";
export const NOTIFICATION_TYPE_WIN: string = "BOOKING_ADVICE";

export const PAYMENT_CASH = "efectivo";
export const PAYMENT_CARD = "tarjeta";

