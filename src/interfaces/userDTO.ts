import {Location} from "./location";

/**
 * UserDTO interface used to manage data received from a push notification with information about uid, onesignal id, name, usenname, profile photo path and phone number.
 */
export interface UserDTO {
  uid: string,
  oneSignalId: string,
  name: string,
  userName: string,
  photoPath: string,
  phoneNumber: string
}
