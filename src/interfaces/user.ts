import {Location} from "./location";

/**
 * User interface used to manage data inside the app as uid, name, lastname, email, phone, password and location
 */
export interface User {
  uid: string,
  name: string,
  lastname: string,
  email: string,
  phone: string,
  password: string,
  location: Location
}
