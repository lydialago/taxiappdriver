import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {TabsPage} from "../tabs/tabs";

/**
 * The component for AjustesPage. Allows the driver to modify the settings of the driver's account (data, notifications, profile picture, etc.)
 */
@Component({
  selector: 'page-ajustes',
  templateUrl: 'ajustes.html',
})
export class AjustesPage {

  /**
   * Constructor of AjustesPage
   *
   * @param {NavController} navCtrl Base class to navigate between pages
   */
  constructor(public navCtrl: NavController) {}

  /**
   * Function to go back to HomePage and set it root.
   * @returns void
   */
  activarPrincipal(){
    this.navCtrl.setRoot(TabsPage);
  }

}
