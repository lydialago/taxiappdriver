import { Component } from '@angular/core';
import {AlertController, MenuController, NavController, NavParams, ToastController} from 'ionic-angular';
import * as Constants from "../../interfaces/constants";

//interfaces
import { Servicio } from "../../interfaces/servicio.interface";
import { SERVICIOS } from "../../data/data.servicios";

//provider
import { ConvertirFechaStringProvider } from "../../providers/convertir-fecha-string/convertir-fecha-string";
import { OrdenarServiciosProvider } from "../../providers/ordenar-servicios/ordenar-servicios";
import {ObtenerPendientesProvider} from "../../providers/obtener-pendientes/obtener-pendientes";

/**
 * Component for the PendientesPage. It shows a list of pendings services not assigned that the driver can accept.
 */
@Component({
  selector: 'page-pendientes',
  templateUrl: 'pendientes.html',
})
export class PendientesPage {

  /** Array of pending services. */
  servicios : Servicio[] = [];
  /** String "efectivo". */
  paymentCash: string = Constants.PAYMENT_CASH;
  /** String "tarjeta". */
  paymentCard: string = Constants.PAYMENT_CARD;

  /**
   * Constructor of PendientesPage.
   *
   * @param {NavController} navCtrl Base class to navigate between pages.
   * @param {AlertController} alertCtrl Default component which is a dialog that presents users with information or collects information from the user using inputs.
   * @param {MenuController} menuCtrl Default provider which makes it easy to control a Menu. Its methods can be used to display the menu, enable the menu, toggle the menu, and more.
   * @param {ToastController} toastCtrl Default component for subtle notification commonly used in modern applications. It can be used to provide feedback about an operation or to display a system message.
   * @param {NavParams} navParams Object that exists in the page and contain data from another view that it has passed
   * @param {ConvertirFechaStringProvider} _convertirFechaString
   * @param {OrdenarServiciosProvider} _ordenarServicios Custom provider to sort services by datetime.
   * @param {ObtenerPendientesProvider} _obtenerPendientes Custom provider to get all the pending services from the server.
   */
  constructor(
      public navCtrl: NavController,
      public alertCtrl: AlertController,
      public menuCtrl: MenuController,
      private toastCtrl: ToastController,
      public navParams: NavParams,
      public _convertirFechaString: ConvertirFechaStringProvider,
      public _ordenarServicios: OrdenarServiciosProvider,
      public _obtenerPendientes: ObtenerPendientesProvider
  ) {
    this._obtenerPendientes.obtenerServiciosPendientes()
      .then(data => {
        this.servicios = this._ordenarServicios.ordenarServiciosPrimeroFuturos(data)
      })
      .catch( error => {
        console.log(error);
        this.toastCtrl.create({
          message: 'No se han podido obtener los servicios pendientes.',
          showCloseButton : true,
          position: 'bottom'
        }).present();
      });
  }

  /**
   * TODO to develop when the logic is implemented on the server
   * Function to accept pending services that have not found a driver
   *
   * @param servicio Service to accept
   * @param i Position in the array of services
   */
  aceptarCarrera(servicio, i){
      console.log(servicio);
      console.log(i);
      let alert = this.alertCtrl.create({
          title: '¿Aceptas el servicio?',
          message: '<p>Punto de recogida:</p><p><strong>'
          + servicio.origin.address +
          '</strong></p><p>Hora: <strong>' +  servicio.pickupAt + '</strong></p>',
          buttons: [
              {
                  text: 'Cancelar',
                  role: 'cancel',
                  handler: () => {
                      console.log('Cancel clicked');
                  }
              },
              {
                  text: 'Aceptar',
                  handler: () => {
                      console.log('Aceptar clicked');
                  }
              }
          ]
      });
      alert.present();
  }

  /**
   * Function that uses the menu controller and shows or hides the menu panel
   * @returns void
   */
  mostrarMenu() {
      this.menuCtrl.toggle();
  }

  /**
   * Function to get the pending services when the driver refresh the list of services
   * @param refresher Component that provides pull-to-refresh functionality on a content component.
   * @returns void
   */
  doRefresh(refresher) {
    this._obtenerPendientes.obtenerServiciosPendientes()
      .then(data => {
        this.servicios = this._ordenarServicios.ordenarServiciosPrimeroFuturos(data)
        refresher.complete();
      })
      .catch( error => {
        console.log(error);
        this.toastCtrl.create({
          message: 'No se han podido obtener los servicios pendientes.',
          showCloseButton : true,
          position: 'bottom'
        }).present();
      });
  }

}
