import {Component, ElementRef, ViewChild} from '@angular/core';
import {NavController, NavParams, Platform, AlertController, MenuController} from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import {SERVICIOS} from "../../data/data.servicios";

/* Interfaces */
import {Servicio} from "../../interfaces/servicio.interface";
import {googleMapsStyle} from "../../environments/environment";

/* Providers */
import { ConvertirFechaStringProvider } from "../../providers/convertir-fecha-string/convertir-fecha-string";
import { OrdenarServiciosProvider } from "../../providers/ordenar-servicios/ordenar-servicios";
import * as Constants from "../../interfaces/constants";
import { DriverInfoProvider } from "../../providers/driver-info/driver-info";
import * as firebase from 'firebase/app';

declare var google: any;

/**
 * Component for the MisServiciosPage.
 */
@Component({
    selector: 'page-mis-servicios',
    templateUrl: 'mis-servicios.html',
})
export class MisServiciosPage {

  /** The reference of the map in the html. */
  @ViewChild('map') mapElement: ElementRef;
  /** The Google Maps map. */
  map: any;
  /** Array that contains the driver's marker and of all the services accepted by him and to be done. */
  markers: any = [];
  /** Array of driver's services accepted but not done. */
  servicios : Servicio[] = [];
  /** String "efectivo". */
  paymentCash: string = Constants.PAYMENT_CASH;
  /** String "tarjeta". */
  paymentCard: string = Constants.PAYMENT_CARD;

/**
 * Constructor of MisServiciosPage. It shows a list with all the services accepted by the driver to be done and their situation in a map.
 *
 * @param {NavController} navCtrl Base class to navigate between pages.
 * @param {AlertController} alertCtrl Default component which is a dialog that presents users with information or collects information from the user using inputs.
 * @param {MenuController} menuCtrl Default provider which makes it easy to control a Menu. Its methods can be used to display the menu, enable the menu, toggle the menu, and more.
 * @param {NavParams} navParams Object that exists in the page and contain data from another view that it has passed
 * @param {Platform} platform Default service used to get information about the current device.
 * @param {Geolocation} geolocation Plugin that provides information about the device's location, such as latitude and longitude.
 * @param {ConvertirFechaStringProvider} _convertirFechaString Custom provider to transform a datetime string into a sentence like "Hoy a las 12:00".
 * @param {OrdenarServiciosProvider} _ordenarServicios Custom provider to sort services by datetime.
 * @param {DriverInfoProvider} _driverInfo Custom provider to create a driver object, set and obtain information.
 */
  constructor(
      public navCtrl: NavController,
      public alertCtrl: AlertController,
      public menuCtrl: MenuController,
      public navParams: NavParams,
      public platform: Platform,
      public geolocation: Geolocation,
      public _convertirFechaString: ConvertirFechaStringProvider,
      public _ordenarServicios: OrdenarServiciosProvider,
      public _driverInfo: DriverInfoProvider
  ) {
      this.servicios = SERVICIOS.slice(0);
      this.servicios = this._ordenarServicios.ordenarServiciosPrimeroPasados(this.servicios);
  }

  /**
   * Function called when loading the page and loads the map.
   * @returns void
   */
  ionViewDidLoad() {
      this.loadMap();
  }

  /**
   * Function that creates a map with the Google Maps API and also creates the markers with the position of the the driver in real time and sets the markers of all services accepted by him and to be done. Position coordinates are obtained and updated from Firebase.
   * @returns void
   */
  loadMap() {

      this.platform.ready().then(() => {

        let latLngDriver = new google.maps.LatLng(this._driverInfo.driver.location.lat, this._driverInfo.driver.location.lng);

        let mapOptions = {
            center: latLngDriver,
            zoom: 15,
            tilt: 0,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            disableDoubleClickZoom: false,
            disableDefaultUI: true,
            zoomControl: false,
            scaleControl: false,
            styles: googleMapsStyle
        };

        this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

        let marker = new google.maps.Marker({
          position: latLngDriver,
          icon: 'assets/imgs/taxi16.png'
        });
        this.markers.push({
          role : Constants.DRIVER_ROLE,
          marker : marker
        });

        this.setMapOnAll(this.map);

        firebase.database()
          .ref('/drivers')
          .child(this._driverInfo.driver.uid).on('value', data => {
          let driverLocation = new google.maps.LatLng(data.val().location.lat,data.val().location.lng);
          this.updateDriverMarker(driverLocation);
        });

        this.cargarServiciosMap();
      });
  }

/**
 * Function to add a marker to the marker's array with an icon and specific information window.
 * @param location Coordinates of the service pick up point.
 * @param image String with the route of the icon for the marker.
 * @param content String with the html content to be set in the information window.
 * @returns void
 */
  addMarker(location, image, content) {
      let marker = new google.maps.Marker({
          position: location,
          map: this.map,
          icon: image
      });

      this.addInfoWindow(marker, content);

      this.markers.push(marker);
  }

/**
 * Function to update the position of the driver's marker (marker in position 0 of the array).
 * @param position Coordinates of the driver.
 */
  updateDriverMarker(position){
      this.markers[0].marker.setPosition(position);
  }

  /**
   * Function to set on the map all the markers from the array.
   * @param map The Google Maps map where the markers are placed.
   */
  setMapOnAll(map) {
      for (var i = 0; i < this.markers.length; i++) {
          this.markers[i].marker.setMap(map);
      }
  }

  /**
   * Function to delete all markers from the map
   * @returns void
   */
  clearMarkers() {
      this.setMapOnAll(null);
      this.markers = [];
  }

  /**
   * Function that goes over the array of services and calls another function to create the marker and add it to the array of markers.
   * @returns void
   */
  cargarServiciosMap(){
      this.servicios.forEach( servicio => {
          let servicioLocation = new google.maps.LatLng(servicio.origin.lat, servicio.origin.lng);
          let image = 'assets/imgs/marker.png';
          let content = "<h5>" + servicio.origin.address + "</h5><p>" + this._convertirFechaString.formateoHoraRecogida(servicio) + "</p><p>Método de pago: " + servicio.bookingOptions.payment + "</p>";
          this.addMarker(servicioLocation, image, content);
      });
  }

  /**
   * Function that creates an information window and assigns it to a marker of the map.
   * @param marker Marker to which the window is added.
   * @param content Information to be diplayed on the window.
   */
  addInfoWindow(marker, content){
      let infoWindow = new google.maps.InfoWindow({
          content: content
      });

      google.maps.event.addListener(marker, 'click', () => {
          infoWindow.open(this.map, marker);
      });
  }

  /**
   * Function to mark on the html list of services which are delayed.
   * @param {Servicio} servicio Service that is checked.
   * @returns {boolean}
   */
  serviciosPasados(servicio: Servicio){
      let currentDate = new Date();
      return Date.parse(servicio.pickupAt) < currentDate.getTime();
  }

  /**
   * Function that uses the menu controller and shows or hides the menu panel
   * @returns void
   */
  mostrarMenu() {
      this.menuCtrl.toggle();
  }
}
