import { Component } from '@angular/core';
import {NavController, MenuController, AlertController, ModalController, ToastController} from 'ionic-angular';
import { SERVICIOS } from "../../data/data.servicios";
import { OneSignal } from "@ionic-native/onesignal";

//interfaces
import { Servicio } from "../../interfaces/servicio.interface";
import * as Constants from "../../interfaces/constants";

//provider
import { ConvertirFechaStringProvider } from "../../providers/convertir-fecha-string/convertir-fecha-string";
import { OrdenarServiciosProvider } from "../../providers/ordenar-servicios/ordenar-servicios";
import { DetalleServicioPage } from "../detalle-servicio/detalle-servicio";
import { DriverInfoProvider } from "../../providers/driver-info/driver-info";
import {TabsPage} from "../tabs/tabs";
import {PushNotificationsProvider} from "../../providers/push-notifications/push-notifications";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {ObtenerPendientesProvider} from "../../providers/obtener-pendientes/obtener-pendientes";

/**
 * The component for the HomePage, the main page. It shows some information about the driver, pending services are obtained from the server and displayed, the driver's status can be modified (busy, free or break), pending or scheduled services are accepted and new service push notifications are received.
 */
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  /** Array with all the driver status */
  allEstados: number[] = [ Constants.STATUS_OFF, Constants.STATUS_BUSY, Constants.STATUS_FREE, Constants.STATUS_BREAK ];
  /** Boolean use for the break button to determine if the driver's in a break. */
  descanso: boolean;
  /** String with the color of the break button that changes. */
  descansoColor: string;
  /** String with the text of the break button that changes. */
  descansoString: string;
  /** String "efectivo". */
  paymentCash: string = Constants.PAYMENT_CASH;
  /** String "tarjeta". */
  paymentCard: string = Constants.PAYMENT_CARD;
  /** Array of services. */
  servicios : Servicio[] = [];
  notificacionSubscribe;

  /**
   * Constructor of the HomePage
   *
   * @param {MenuController} menuCtrl Default provider which makes it easy to control a Menu. Its methods can be used to display the menu, enable the menu, toggle the menu, and more.
   * @param {AlertController} alertCtrl Default component which is a dialog that presents users with information or collects information from the user using inputs.
   * @param {ToastController} toastCtrl Default component for subtle notification commonly used in modern applications. It can be used to provide feedback about an operation or to display a system message.
   * @param {ModalController} modalCtrl Default component which is a content pane that goes over the user's current page.
   * @param {ConvertirFechaStringProvider} _convertirFechaString Custom provider to transform a datetime string into a sentence like "Hoy a las 12:00".
   * @param {OrdenarServiciosProvider} _ordenarServicios Custom provider to sort services by datetime.
   * @param {DriverInfoProvider} _driverInfo Custom provider to create a driver object, set and obtain information.
   * @param {PushNotificationsProvider} _pushNotif Custom provider to manage push notifications.
   * @param {ObtenerPendientesProvider} _obtenerPendientes Custom provider to get all the pending services from the server.
   * @param {HttpClient} http Cordova / Phonegap plugin for communicating with HTTP servers.
   */
  constructor(
    public menuCtrl: MenuController,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public modalCtrl: ModalController,
    public _convertirFechaString: ConvertirFechaStringProvider,
    public _ordenarServicios: OrdenarServiciosProvider,
    public _driverInfo: DriverInfoProvider,
    public _pushNotif: PushNotificationsProvider,
    public _obtenerPendientes: ObtenerPendientesProvider,
    public http: HttpClient,
  ) {
    this.descansoColor = "disabled";
    this.descansoString = "descanso";
    this._obtenerPendientes.obtenerServiciosPendientes()
      .then(data => {
        this.servicios = this._ordenarServicios.ordenarServiciosPrimeroFuturos(data)
      })
      .catch( error => {
        console.log(error);
        this.toastCtrl.create({
          message: 'No se han podido obtener los servicios pendientes.',
          showCloseButton : true,
          position: 'bottom'
        }).present();
      });


    let notificacionSubscribe = this._pushNotif.notificationObservable;
    notificacionSubscribe.subscribe(data => {
      console.log("observable del home problematico " + JSON.stringify(data));

      if('shouldLeave' in data || 'lost' in data){

      } else {
        this.servicios = data.pendingServices;
        this.servicios = this._ordenarServicios.ordenarServiciosPrimeroFuturos(this.servicios);
      }

    });
  }

  /**
   * Function that is triggered when the driver presses the button to change status. Grab the current driver status (free or busy) and update it to the contrary in Firebase and in the server database.
   * @returns void
   */
  cambiarEstado(){
    if(this._driverInfo.driver.status === Constants.STATUS_FREE){
      this._driverInfo.updateStatus(Constants.STATUS_BUSY);
    }
    else if(this._driverInfo.driver.status === Constants.STATUS_BUSY){
      this._driverInfo.updateStatus(Constants.STATUS_FREE);
    }
  }

  /**
   * Function that is triggered when the driver presses the rest button. It takes the current state of the driver (resting or not) and updates it to the contrary in Firebase and in the database of the server.
   * @returns void
   */
  cambiarDescanso(){
    if(this._driverInfo.driver.status === Constants.STATUS_BREAK){
      this._driverInfo.updateStatus(Constants.STATUS_FREE);
        this.descansoColor = "disabled";
        this.descansoString = "descanso";
    }
    else if(this._driverInfo.driver.status === Constants.STATUS_FREE){
      this._driverInfo.updateStatus(Constants.STATUS_BREAK);
      this.descansoColor = "secondary";
      this.descansoString = "descansando";
    }
  }

  /**
   * TODO to develop when the logic is implemented on the server
   * Function to accept pending services that have not found a driver
   *
   * @param servicio Service to accept
   * @param i Position in the array of services
   */
  aceptarCarrera(servicio, i){
    console.log(servicio);
    console.log(i);
    let alert = this.alertCtrl.create({
        title: '¿Aceptas el servicio?',
        message: '<p>Punto de recogida:</p><p><strong>'
        + servicio.origin.address +
        '</strong></p><p>Hora: <strong>' +  this._convertirFechaString.formateoHoraRecogida(servicio) + '</strong></p>',
        buttons: [
            {
                text: 'Cancelar',
                role: 'cancel',
                handler: () => {
                    console.log('Cancel clicked');
                }
            },
            {
                text: 'Aceptar',
                handler: () => {
                    let modal = this.modalCtrl.create( DetalleServicioPage, servicio);
                    modal.present();
                }
            }
        ]
    })
    // alert.present();
  }

  /**
   * Function that uses the menu controller and shows or hides the menu panel
   * @returns void
   */
  mostrarMenu() {
    this.menuCtrl.toggle();
  }

  /**
   * Function to get the pending services when the driver refresh the list of services
   * @param refresher Component that provides pull-to-refresh functionality on a content component.
   * @returns void
   */
  doRefresh(refresher) {
    this._obtenerPendientes.obtenerServiciosPendientes()
      .then(data => {
        this.servicios = this._ordenarServicios.ordenarServiciosPrimeroFuturos(data);
        refresher.complete();
      })
      .catch( error => {
        console.log(error);
        this.toastCtrl.create({
          message: 'No se han podido obtener los servicios pendientes.',
          showCloseButton : true,
          position: 'bottom'
        }).present();
      });
  }

}
