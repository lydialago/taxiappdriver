import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {TabsPage} from "../tabs/tabs";

/**
 * The component for HistorialPage. Shows the driver information and metrics about his activity and services done.
 */
@Component({
  selector: 'page-historial',
  templateUrl: 'historial.html',
})
export class HistorialPage {

  /**
   * Constructor of the HistorialPage
   *
   * @param {NavController} navCtrl
   */
  constructor(public navCtrl: NavController) {}

  /**
   * Function to go back to HomePage and set it root.
   * @returns void
   */
  activarPrincipal(){
    this.navCtrl.setRoot(TabsPage);
  }

}
