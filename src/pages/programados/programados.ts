import { Component } from '@angular/core';
import {AlertController, MenuController, NavController, NavParams} from 'ionic-angular';
import * as Constants from "../../interfaces/constants";

//interfaces
import { Servicio } from "../../interfaces/servicio.interface";
import { SERVICIOS } from "../../data/data.servicios";

//provider
import { ConvertirFechaStringProvider } from "../../providers/convertir-fecha-string/convertir-fecha-string";
import { OrdenarServiciosProvider } from "../../providers/ordenar-servicios/ordenar-servicios";

/**
 * Component for the ProgramadosPage. It shows a list of scheduled services not assigned that the driver can accept.
 */
@Component({
  selector: 'page-programados',
  templateUrl: 'programados.html',
})
export class ProgramadosPage {

  /** Array of scheduled services. */
  servicios : Servicio[] = [];
  /** String "efectivo". */
  paymentCash: string = Constants.PAYMENT_CASH;
  /** String "tarjeta". */
  paymentCard: string = Constants.PAYMENT_CARD;

  /**
   * Constructor of ProgramadosPage
   *
   * @param {AlertController} alertCtrl Default component which is a dialog that presents users with information or collects information from the user using inputs.
   * @param {MenuController} menuCtrl Default provider which makes it easy to control a Menu. Its methods can be used to display the menu, enable the menu, toggle the menu, and more.
   * @param {ConvertirFechaStringProvider} _convertirFechaString.
   * @param {OrdenarServiciosProvider} _ordenarServicios Custom provider to sort services by datetime.
   */
  constructor(
    public alertCtrl: AlertController,
    public menuCtrl: MenuController,
    public _convertirFechaString: ConvertirFechaStringProvider,
    public _ordenarServicios: OrdenarServiciosProvider
  ) {
    this.servicios = SERVICIOS.slice(0);
      this.filtrarPorFechaHora();
      this.servicios = this._ordenarServicios.ordenarServiciosPrimeroFuturos(this.servicios);
  }

  /**
   * TODO to delete when the logic of accept scheduled services is implemented on the server. The scheduled services are obtained from a data file not from the server.
   */
  filtrarPorFechaHora(){
    let fechaHoraActual = new Date();
    this.servicios = this.servicios.filter((servicio) => Date.parse(servicio.pickupAt) >= fechaHoraActual.getTime());
  }

  /**
   * TODO to develop when the logic is implemented on the server
   * Function to accept pending services that have not found a driver
   *
   * @param servicio Service to accept
   * @param i Position in the array of services
   */
  aceptarCarrera(servicio, i){
    console.log(servicio);
    console.log(i);
    let alert = this.alertCtrl.create({
      title: '¿Aceptas el servicio?',
      message: '<p>Punto de recogida:</p><p><strong>'
      + servicio.origin.address +
      '</strong></p><p>Hora: <strong>' +  servicio.pickupAt + '</strong></p>',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
            console.log('Aceptar clicked');
          }
        }
      ]
    });
    alert.present();
  }

  /**
   * Function that uses the menu controller and shows or hides the menu panel
   * @returns void
   */
  mostrarMenu() {
    this.menuCtrl.toggle();
  }

}
