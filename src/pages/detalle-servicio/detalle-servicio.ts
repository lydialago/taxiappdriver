import { Component, ElementRef, ViewChild } from '@angular/core';
import {AlertController, NavController, NavParams, Platform, ToastController} from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import * as firebase from 'firebase/app';

/** Interfaces */
import { Servicio } from "../../interfaces/servicio.interface";
import * as Constants from "../../interfaces/constants";
import { User } from "../../interfaces/user";
import {googleMapsStyle} from "../../environments/environment";

/** Paginas */
import { TabsPage } from "../tabs/tabs";

/** Providers */
import { CallProvider } from "../../providers/call/call";
import { DriverInfoProvider } from "../../providers/driver-info/driver-info";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import {PushNotificationsProvider} from "../../providers/push-notifications/push-notifications";

declare var google: any;

/**
 * The component for DetalleServicioPage. It shows all the information related to the service that the driver has accepted. It shows the following information: map with the client's location and its own in real time, origin, destination and datetime of the service, point of pickup of the client, etc. It also allows the driver to call the customer, warn that you have arrived or that you are leaving if the client is not there.
 */
@Component({
  selector: 'page-detalle-servicio',
  templateUrl: 'detalle-servicio.html',
})

export class DetalleServicioPage {

    /** The reference of the map in the html. */
    @ViewChild('map') mapElement: ElementRef;
    /** The Google Maps map. */
    map: any;
    /** Boolean that determines if the driver has warned the customer that he is at the pickup point. */
    ondoorbutton: boolean;
    /** Boolean that determines if the driver has warned the customer and he's leaving. */
    leavingbutton: boolean;
    /** Object with all the information about the service (origin, destination, pickup datetime, payment...*/
    servicio: Servicio;
    /** String of the service's deal id. */
    dealId: string;
    /** Array that contains the customer and the driver's markers. */
    markers: { role: string, marker: any } [] = [];
    /** Boolean that determines if the driver has picked up the customer and the service starts. */
    enCarrera: boolean;
    /** Object with all the information about the customer. */
    user = {} as User;

  /**
   * Constructor of the DetalleServicioPage.
   *
   * @param {NavController} navCtrl Base class to navigate between pages.
   * @param {NavParams} navParams Object that exists in the page and contain data from another view that it has passed.
   * @param {CallProvider} _call Custom provider for calling.
   * @param {Platform} platform Default service used to get information about the current device.
   * @param {Geolocation} geolocation Plugin that provides information about the device's location, such as latitude and longitude.
   * @param {DriverInfoProvider} _driverInfo Custom provider to create a driver object, set and obtain information.
   * @param {HttpClient} http Cordova / Phonegap plugin for communicating with HTTP servers.
   * @param {PushNotificationsProvider} _pushNotif Custom provider to manage push notifications.
   * @param {ToastController} toastCtrl Default component for subtle notification commonly used in modern applications. It can be used to provide feedback about an operation or to display a system message.
   */
  constructor(
      public navCtrl: NavController,
      public navParams: NavParams,
      public _call: CallProvider,
      public platform: Platform,
      public geolocation: Geolocation,
      public _driverInfo: DriverInfoProvider,
      public http: HttpClient,
      public _pushNotif: PushNotificationsProvider,
      public toastCtrl: ToastController
  ) {
      this.ondoorbutton = false;
      this.ondoorbutton = false;
      this.servicio = this.navParams.data.servicio;
      console.log("El servicio es " + JSON.stringify(this.servicio));
      this.dealId = this.navParams.data.dealId;
      console.log("El dealId es " + this.dealId);
      this.enCarrera = false;
    }

  /**
   * Function called when loading the page and do the following: load the map, set the client's location and subscribe to an observable variable that determines if the service has been canceled (if the client has canceled it or because the client does not appear at the pick up point so the driver leaves) and if it's true, it returns to the main screen.
   * @returns void
   */
  ionViewDidLoad() {
        this.loadMap();
        this.user.location = { lat: 0, lng: 0, address : ""};

        let notificacionSubscribe = this._pushNotif.notificationObservable;
        notificacionSubscribe.subscribe(data => {
            console.log("notificacion observable " + JSON.stringify(data));
            if(data.lost || data.shouldLeave){
                this.navCtrl.setRoot(TabsPage);
                notificacionSubscribe.unsubscribe();
            }
        });
    }

  /**
   * Function that creates a map with the Google Maps API and also creates two markers with the position of the client and the driver in real time. Position coordinates are obtained from Firebase.
   * @returns void
   */
  loadMap() {

        this.platform.ready().then(() => {

          let latLngClient = new google.maps.LatLng(this.servicio.origin.lat, this.servicio.origin.lng);

          let mapOptions = {
              center: latLngClient,
              zoom: 16,
              tilt: 0,
              mapTypeId: google.maps.MapTypeId.ROADMAP,
              disableDoubleClickZoom: false,
              disableDefaultUI: true,
              zoomControl: false,
              scaleControl: false,
              styles: googleMapsStyle
          };
            this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

            // insert the marker of the service pick-up point
            let marker = new google.maps.Marker({
                position: latLngClient,
                icon: 'assets/imgs/pin.png',
            });

            marker.setMap(this.map);

            // insert the marker of the client's position
            marker = new google.maps.Marker({
              position: latLngClient,
              icon: 'assets/imgs/marker.png'
            });
            this.markers.push({
              role : Constants.CLIENT_ROLE,
              marker : marker
            });

            let latLngDriver = new google.maps.LatLng(this._driverInfo.driver.location.lat, this._driverInfo.driver.location.lng);

            // insert the marker of the driver's position
            marker = new google.maps.Marker({
              position: latLngDriver,
              icon: 'assets/imgs/taxi16.png'
            });
            this.markers.push({
              role : Constants.DRIVER_ROLE,
              marker : marker
            });

            this.setMapOnAll(this.map);

            // the markers are updated every time the client or the driver moves
            firebase.database()
              .ref('/drivers')
              .child(this._driverInfo.driver.uid).on('value', data => {
                let driverLocation = new google.maps.LatLng(data.val().location.lat,data.val().location.lng);
                this.updateDriverMarker(Constants.DRIVER_ROLE, driverLocation);
            });

            firebase.database()
                .ref('/users')
                .child(this.servicio.userId).on('value', data => {
                let userLocation = new google.maps.LatLng(data.val().location.lat,data.val().location.lng);
                this.updateDriverMarker(Constants.CLIENT_ROLE, userLocation);
                this.map.setCenter(new google.maps.LatLng(data.val().location.lat, data.val().location.lng));
            });

        });
    }

  /**
   * Function to update the coordinates of the customer or driver's markers
   * @param role A constant that it can be driver or client
   * @param position The coordinates of the marker
   */
    updateDriverMarker(role, position){
        this.markers.forEach( (element) => {
            if(element.role === role){
                element.marker.setPosition(position);
            }
        });
        // this.setMapOnAll(this.map);
    }

  /**
   * Function to place all the markers of the array on the map
   * @param map The Google Maps map created
   * @returns void
   */
    setMapOnAll(map) {
        this.markers.forEach( (element) => {
            element.marker.setMap(map);
        })
    }

  /**
   * Function to call the customer using CallProvider
   * @returns void
   */
    llamarCliente(){
        this._call.llamar(this.servicio.user.phoneNumber);
    }

  /**
   * Function to warn the customer that the driver has arrived at the pick up point. It makes a post request to the server
   * @returns void
   */
    estoyAqui(){
        this.ondoorbutton = true;
        this.http.post(Constants.SERVER_IP + "/deal/" + this.dealId + "/on-door/", null, {
          headers: new HttpHeaders().set('Content-Type', 'application/json')
        }).subscribe((res: any) => {
          console.log(res);
        }, (err) => {
          console.log(err);
          let toast = this.toastCtrl.create({
            message: 'No se ha podido avisar al cliente de que has llegado. Inténtalo de nuevo más tarde.',
            showCloseButton : true,
            position: 'bottom'
          });
          toast.present();
          this.ondoorbutton = false;
        });
    }

  /**
   * Function to warn the customer that the driver has arrived at the pick up point and he's leaving because the customer isn't there. It makes a post request to the server
   * @returns void
   */
    meVoy(){
      this.leavingbutton = true;
      this.http.post(Constants.SERVER_IP + "/deal/" + this.dealId + "/leave/", null,{
        headers: new HttpHeaders().set('Content-Type', 'application/json')
      }).subscribe((res: any) => {
        console.log(res);
      }, (err) => {
        console.log(err);
        let toast = this.toastCtrl.create({
          message: 'No se ha podido avisar al cliente de que te vas. Inténtalo de nuevo más tarde.',
          showCloseButton : true,
          position: 'bottom'
        });
        toast.present();
        this.leavingbutton = false;
      });
    }

  /**
   * Function to change the status of the deal when the customer is picked up and the service begins. It also makes a post request to the server
   * @returns void
   */
    inicioCarrera(){
        this.enCarrera = true;
        this.http.post(Constants.SERVER_IP + "/deal/" + this.dealId + "/pickedUp/", null, {
          headers: new HttpHeaders().set('Content-Type', 'application/json')
        }).subscribe((res: any) => {
          console.log(res);
        }, (err) => {
          console.log(err);
          let toast = this.toastCtrl.create({
            message: 'No se ha podido iniciar la carrera. Inténtalo de nuevo más tarde.',
            showCloseButton : true,
            position: 'bottom'
          });
          toast.present();
        });
    }

  /**
   * Function to change the status of the deal when the service is finished. The driver's status is set to free and returns to the main page
   * @returns void
   */
  finalizarCarrera(){
      this.http.post(Constants.SERVER_IP + "/deal/" + this.dealId + "/win/", null,{
        headers: new HttpHeaders().set('Content-Type', 'application/json')
      }).subscribe((res: any) => {
        console.log(res);
      }, (err) => {
        console.log(err);
        let toast = this.toastCtrl.create({
          message: 'No se ha podido finalizar la carrera.',
          showCloseButton : true,
          position: 'bottom'
        });
        toast.present();
      });
      this._driverInfo.updateStatus(Constants.STATUS_FREE);
      this.navCtrl.setRoot(TabsPage);
    }

}
