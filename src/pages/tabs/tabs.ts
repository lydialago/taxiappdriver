import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

//paginas
import { HomePage } from "../home/home";
import { HistorialPage } from "../historial/historial";
import { MisServiciosPage } from "../mis-servicios/mis-servicios";
import { PendientesPage } from "../pendientes/pendientes";
import { ProgramadosPage } from "../programados/programados";

/**
 * Component for the TabsPage. The purpose of this component is to show the tabs on the bottom of the screen.
 */
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {

  /** Reference to HomePage */
  homePage: any = HomePage;
  /** Reference to MisServiciosPage */
  misServiciosPage: any = MisServiciosPage;
  /** Reference to PendientesPage */
  pendientesPage: any = PendientesPage;
  /** Reference to ProgramadosPage */
  programadosPage: any = ProgramadosPage;

  /**
   * Empty constructor of the TabsPage
   */
  constructor() {}

}
