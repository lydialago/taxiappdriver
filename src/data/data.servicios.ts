export const SERVICIOS = [
    {
      userId: "nZoOqAGqh1XY1CkmeXboIVtCSr63",
      user: {
        uid: "nZoOqAGqh1XY1CkmeXboIVtCSr63",
        oneSignalId: "nZoOqAGqh1XY1CkmeXboIVtCSr63",
        name: "Lydia Lago",
        userName: "lydia",
        photoPath: "",
        phoneNumber: "600359514"
      },
      pickupNow: true,
      origin: {
        lat: 41.414612,
        lng: 2.208109,
        address: "Calle Josep Pla 107, Barcelona",
      },
      destination: {
        lat: 41.375054,
        lng: 2.162486,
        address: "Avinguda Paral·lel 45"
      },
      pickupAt: "2018-06-10 16:30:00",
      bookingOptions : {
        payment: "tarjeta",
        drivingType: "normal",
        premiumDriver: false,
        bigCar: false
      }
    },
    {
    userId: "nZoOqAGqh1XY1CkmeXboIVtCSr63",
    user: {
      uid: "nZoOqAGqh1XY1CkmeXboIVtCSr63",
      oneSignalId: "nZoOqAGqh1XY1CkmeXboIVtCSr63",
      name: "Lydia Lago",
      userName: "lydia",
      photoPath: "",
      phoneNumber: "600359514"
    },
    pickupNow: true,
    origin: {
      lat: 41.466507,
      lng: 2.247213,
      address: "Calle Sitges 17, Badalona",
    },
    destination: {
      lat: null,
      lng: null,
      address: null
    },
    pickupAt: "2018-06-08 12:01:00",
    bookingOptions : {
      payment: "efectivo",
      drivingType: "normal",
      premiumDriver: false,
      bigCar: false
    }
  },
];
